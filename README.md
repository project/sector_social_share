The **Sector Social Share module** — part of the Starter Kit in Sector 10 — is built on the Drupal core Block module and the Drupal contribution module: <a href="https://www.drupal.org/project/addtoany" target="_blank">AddtoAny</a>.

<h2>Sector Social Share includes:</h2>
<ul>
  <li>an opt-in module AddToAny share buttons module and block which is placed in the block layout when the Sector Social Share module is enabled. </li>

  <li>The block is placed last in the main content region.</li>

  <li>We added three default, customisable "service buttons" with a dark theme.</li>
</ul>

<h3>Related entities and configuration </h3>
<ul>
  <li>/admin/structure/block/manage/sector_starter_addtoanysharebuttons</li>
  <li>/admin/config/services/addtoany</li>
</ul>

Sector permissions for AddToAny
<ul>
  <li>/admin/people/permissions#module-addtoany</li>
</ul>

Features and Functionality
<ul>
  <li>AddToAny's universal sharing button with SVG icons for many services like Facebook, Twitter, Pinterest, WhatsApp, Reddit, SMS, email and many more.</li>

  <li>The share menu is translated into over 50 languages</li>

  <li>Allows customisation globally or per block.</li>
</ul>

<h3>Further documentation:</h3>
AddtoAny module and https://www.addtoany.com/buttons/customize/drupal.

A worthy read about data privacy when using AddToAny.
TLDR: AddToAny is GDPR compliant. "By design, AddToAny does not store personal data."

<a href="https://www.sector.nz/documentation/sector-social-share-and-follow-sector-10" title="Read more">Documentation</a>
